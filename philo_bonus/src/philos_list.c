/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philos_list.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/22 21:25:59 by davifah           #+#    #+#             */
/*   Updated: 2022/04/06 18:41:27 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"
#include "utils.h"
#include <errno.h>
#include <string.h>
#include <fcntl.h>

static char	*get_sem_name(int n)
{
	char	*sem_name;
	char	*str_n;

	str_n = ft_itoa(n);
	if (!str_n)
		return (NULL);
	sem_name = ft_strjoin("/philo_", str_n);
	free(str_n);
	if (!sem_name)
		return (NULL);
	return (sem_name);
}

t_philo	*init_philos_list(t_arg arg)
{
	t_philo	*philos;
	int		i;
	char	*sem_name;
	sem_t	*fork_sem;

	philos = ft_calloc(arg.number_of_philosophers, sizeof(t_philo));
	if (!philos)
		return (NULL);
	sem_unlink("/philo_fork");
	fork_sem = sem_open("/philo_fork", O_CREAT, 0, arg.number_of_philosophers);
	i = -1;
	while (++i < arg.number_of_philosophers)
	{
		philos[i].fork = fork_sem;
		sem_name = get_sem_name(i);
		if (!sem_name || fork_sem == SEM_FAILED)
			return (NULL);
		sem_unlink(sem_name);
		philos[i].update = sem_open(sem_name, O_CREAT, 0, 1);
		free(sem_name);
		if (philos[i].update == SEM_FAILED)
			return (NULL);
		philos[i].state = sleeping;
	}
	return (philos);
}

void	destroy_philos_list(t_philo *philos, t_arg arg)
{
	int	i;

	i = -1;
	while (++i < arg.number_of_philosophers)
	{
		while (get_state(philos, i) != freed)
			usleep(1000);
		sem_close(philos[i].update);
	}
	sem_close(philos[0].fork);
	free(philos);
}
