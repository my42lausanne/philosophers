/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/22 19:52:32 by davifah           #+#    #+#             */
/*   Updated: 2022/04/06 18:17:30 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"
#include "utils.h"

int	main(int ac, char **av)
{
	t_arg	arg;
	t_philo	*philos;

	arg = parse_args(ac, av);
	if (arg.error)
		return (1);
	philos = init_philos_list(arg);
	if (!philos)
		return (1);
	launch_philos(philos, arg);
	verify_meals(philos, arg);
	destroy_philos_list(philos, arg);
	ft_wait_ms(1);
}
