/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/26 18:40:13 by davifah           #+#    #+#             */
/*   Updated: 2022/04/07 15:17:10 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"

void	set_state(t_philo *philos, int n, t_state state)
{
	sem_wait(philos[n].update);
	philos[n].state = state;
	sem_post(philos[n].update);
}

t_state	get_state(t_philo *philos, int n)
{
	t_state	state;

	state = philos[n].state;
	return (state);
}

void	ft_wait_ms(int n)
{
	long long	start;

	if (n <= 0)
		return ;
	start = get_time_ms();
	while (get_time_ms() - start < n)
		usleep(1000);
}

long long	get_time_ms(void)
{
	struct timeval	tv;

	gettimeofday(&tv, NULL);
	return ((long long)(tv.tv_sec * 1000) + (tv.tv_usec / 1000));
}
