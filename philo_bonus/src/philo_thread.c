/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_thread.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/23 14:56:51 by davifah           #+#    #+#             */
/*   Updated: 2022/04/07 14:45:27 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"
#include "utils.h"

static void		philo_exec_action(t_launch *l, t_state state, int n_eaten);
static t_state	philo_action(t_launch *l, int *n_eaten);
static void		eat(t_launch *l);

void	*philo_start(void *launch)
{
	t_philo	*philos;
	int		n;
	int		n_eaten;
	t_state	state;

	philos = ((t_launch *)launch)->philos;
	n = ((t_launch *)launch)->n;
	n_eaten = 0;
	state = sleeping;
	if (n % 2 != 0)
		ft_wait_ms(3);
	while (state != dead && state != stopping)
	{
		if (((t_launch *)launch)->arg.number_of_philosophers == 1)
		{
			usleep(250);
			state = get_state(philos, n);
		}
		else
			state = philo_action((t_launch *)launch, &n_eaten);
	}
	free(launch);
	set_state(philos, n, freed);
	return (NULL);
}

static t_state	philo_action(t_launch *l, int *n_eaten)
{
	t_state	state;

	sem_wait(l->philos[l->n].update);
	if (l->philos[l->n].state == dead
		|| !l->arg.number_of_times_each_philosopher_must_eat)
		state = dead;
	else if (l->philos[l->n].state == eating)
	{
		l->philos[l->n].state = sleeping;
		state = sleeping;
	}
	else if (0 > l->arg.number_of_times_each_philosopher_must_eat
		|| (*n_eaten)++
		< l->arg.number_of_times_each_philosopher_must_eat - 1)
		state = eating;
	else
		state = stopping;
	sem_post(l->philos[l->n].update);
	philo_exec_action(l, state, *n_eaten);
	return (state);
}

static void	philo_exec_action(t_launch *l, t_state state, int n_eaten)
{
	if (state == eating || state == stopping)
	{
		if (n_eaten > 1)
			printf("%lld %d is thinking\n", get_time_ms(), l->n + 1);
		sem_wait(l->philos[l->n].update);
		if (l->philos[l->n].state == dead)
		{
			sem_post(l->philos[l->n].update);
			return ;
		}
		l->philos[l->n].state = thinking;
		sem_post(l->philos[l->n].update);
		eat(l);
	}
	else if (state == sleeping)
	{
		printf("%lld %d is sleeping\n", get_time_ms(), l->n + 1);
		ft_wait_ms(l->arg.time_to_sleep);
	}
}

static void	eat(t_launch *l)
{
	sem_wait(l->philos[l->n].fork);
	if (get_state(l->philos, l->n) == dead)
	{
		sem_post(l->philos[l->n].fork);
		return ;
	}
	printf("%lld %d has taken a fork\n", get_time_ms(), l->n + 1);
	sem_wait(l->philos[l->n].fork);
	if (get_state(l->philos, l->n) != dead)
		printf("%lld %d has taken a fork\n", get_time_ms(), l->n + 1);
	sem_wait(l->philos[l->n].update);
	if (l->philos[l->n].state != dead)
	{
		l->philos[l->n].state = eating;
		l->philos[l->n].last_meal = get_time_ms();
		sem_post(l->philos[l->n].update);
		printf("%lld %d is eating\n", get_time_ms(), l->n + 1);
		ft_wait_ms(l->arg.time_to_eat);
	}
	else
		sem_post(l->philos[l->n].update);
	sem_post(l->philos[l->n].fork);
	sem_post(l->philos[l->n].fork);
}
