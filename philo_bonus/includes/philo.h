/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/22 19:52:35 by davifah           #+#    #+#             */
/*   Updated: 2022/04/06 18:32:01 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_H
# define PHILO_H

# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <pthread.h>
# include <semaphore.h>
# include "expanded.h"

typedef enum e_state
{
	eating,
	sleeping,
	thinking,
	stopping,
	dead,
	freed
}	t_state;

typedef struct s_arg
{
	int	number_of_philosophers;
	int	time_to_die;
	int	time_to_eat;
	int	time_to_sleep;
	int	number_of_times_each_philosopher_must_eat;
	int	error;
}	t_arg;

typedef struct s_philo
{
	t_state		state;
	sem_t		*update;
	sem_t		*fork;
	long long	last_meal;
}	t_philo;

typedef struct s_launch
{
	int		n;
	t_arg	arg;
	t_philo	*philos;
}	t_launch;

t_arg	parse_args(int ac, char **av);
t_philo	*init_philos_list(t_arg arg);
void	destroy_philos_list(t_philo *philos, t_arg arg);
int		launch_philos(t_philo *philos, t_arg arg);
void	*philo_start(void *launch);
void	verify_meals(t_philo *philos, t_arg arg);

#endif
