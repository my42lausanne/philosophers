/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/26 18:42:28 by davifah           #+#    #+#             */
/*   Updated: 2022/03/27 09:45:37 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTILS_H
# define UTILS_H
# include "philo.h"
# include <sys/time.h>
# include <stdlib.h>

void		set_state(t_philo *philos, int n, t_state state);
t_state		get_state(t_philo *philos, int n);
void		ft_wait_ms(int n);
long long	get_time_ms(void);

#endif
