# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/10/11 12:17:38 by dfarhi            #+#    #+#              #
#    Updated: 2022/03/27 09:48:49 by davifah          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

FILES		= philo arg_parse philos_list philos_launch philo_thread \
			  utils philos_killer

FILES		:= $(addprefix src/, ${FILES})
FILES		:= $(addsuffix .c, ${FILES})
OBJS		= ${FILES:.c=.o}

NAME		= philo

CC			= gcc -Wall -Wextra -Werror

INCLUDES	= -I./includes -I./../libft/includes

LIB			= -L./../libft/ -lft -pthread
LIBFT		= ../libft/libft.a
LIBFT_ARGS	=

SYSTEM		= $(shell uname -s)

${NAME}:	${LIBFT} ${OBJS}
			${CC} ${INCLUDES} -o ${NAME} ${OBJS} ${LIB}

.c.o:
			${CC} -c ${INCLUDES} $< -o ${<:.c=.o}

all:		${NAME}

# cmd to prof code:
# gprof ${NAME} gmon.out > analysis.txt
profile:	fclean
profile:	CC := ${CC} -pg
profile:	LIBFT_ARGS := ${LIBFT_ARGS} PROFILE=1
profile:	${NAME}

${LIBFT}:
			$(MAKE) -C ./../libft expanded ${LIBFT_ARGS}

git:
			@cd ..
			git submodule update --init --recursive

clean:
			rm -f ${OBJS}
			make -C ./../libft clean

fclean:		clean
			rm -f ${NAME} ../libft/libft.a

re:			fclean all

.PHONY:		all clean fclean re
