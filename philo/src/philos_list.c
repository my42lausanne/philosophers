/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philos_list.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/22 21:25:59 by davifah           #+#    #+#             */
/*   Updated: 2022/03/30 22:49:22 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"
#include "utils.h"

t_philo	*init_philos_list(t_arg arg)
{
	t_philo	*philos;
	int		i;

	philos = ft_calloc(arg.number_of_philosophers, sizeof(t_philo));
	if (!philos)
		return (NULL);
	i = -1;
	while (++i < arg.number_of_philosophers)
	{
		pthread_mutex_init(&(philos[i].fork), NULL);
		pthread_mutex_init(&(philos[i].update), NULL);
		philos[i].state = sleeping;
	}
	return (philos);
}

void	destroy_philos_list(t_philo *philos, t_arg arg)
{
	int	i;

	i = -1;
	while (++i < arg.number_of_philosophers)
	{
		while (get_state(philos, i) != freed)
			usleep(1000);
		pthread_mutex_destroy(&(philos[i].fork));
		pthread_mutex_destroy(&(philos[i].update));
	}
	free(philos);
}
